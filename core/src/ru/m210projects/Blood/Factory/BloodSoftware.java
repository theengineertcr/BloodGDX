package ru.m210projects.Blood.Factory;

import static ru.m210projects.Blood.Globals.gFullMap;
import static ru.m210projects.Blood.Globals.gMapScrollMode;
import static ru.m210projects.Blood.Globals.gPlayer;
import static ru.m210projects.Blood.Globals.pGameInfo;
import static ru.m210projects.Blood.View.gViewIndex;
import static ru.m210projects.Build.Engine.MAXPLAYERS;
import static ru.m210projects.Build.Net.Mmulti.connecthead;
import static ru.m210projects.Build.Net.Mmulti.connectpoint2;

import java.util.Arrays;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.Software.Software;

public class BloodSoftware extends Software {

	private int sprites[] = new int[MAXPLAYERS];
	
	public BloodSoftware(Engine engine) {
		super(engine);
		orpho.initmap(24, 24, 0, false, false, false);
	}

	@Override
	public void drawoverheadmap(int cposx, int cposy, int czoom, short cang) {
		Arrays.fill(sprites, -1);
		for (int i = connecthead; i >= 0; i = connectpoint2[i]) 
			sprites[i] = gPlayer[i].nSprite;
		orpho.setmapsettings(gFullMap, pGameInfo.nGameType == 1, gMapScrollMode, gViewIndex, sprites);
		orpho.drawoverheadmap(cposx, cposy, czoom, cang);
	}

}
