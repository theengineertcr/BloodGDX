// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Factory;


import static ru.m210projects.Blood.Globals.gFullMap;
import static ru.m210projects.Blood.Globals.gMapScrollMode;
import static ru.m210projects.Blood.Globals.gPlayer;
import static ru.m210projects.Blood.Globals.pGameInfo;
import static ru.m210projects.Blood.View.gViewIndex;
import static ru.m210projects.Build.Engine.MAXPLAYERS;
import static ru.m210projects.Build.Engine.globalpal;
import static ru.m210projects.Build.Engine.globalshade;
import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.Net.Mmulti.connecthead;
import static ru.m210projects.Build.Net.Mmulti.connectpoint2;

import java.util.Arrays;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.GLFog;
import ru.m210projects.Build.Render.GLInfo;
import ru.m210projects.Build.Render.Polymost.Polymost;
import ru.m210projects.Build.Render.TextureHandle.TextureManager;

public class BloodPolymost extends Polymost {

	private int sprites[] = new int[MAXPLAYERS];

	public BloodPolymost(Engine engine) {
		super(engine);
		orpho.initmap(24, 24, 0, false, false, false);

		globalfog = new GLFog() {
			@Override
			public void calc() {
				super.calc();

				if (pal == 1 && GLInfo.multisample == 0 && textureCache.getShader() == null) { // Blood's pal 1
					start = 0;
					if (end > 2)
						end = 2;
				}
			}
		};
		globalfog.setFogScale(64);
	}

	@Override
	public void drawoverheadmap(int cposx, int cposy, int czoom, short cang) {
		Arrays.fill(sprites, -1);
		for (int i = connecthead; i >= 0; i = connectpoint2[i])
			sprites[i] = gPlayer[i].nSprite;
		orpho.setmapsettings(gFullMap, pGameInfo.nGameType == 1, gMapScrollMode, gViewIndex, sprites);
		orpho.drawoverheadmap(cposx, cposy, czoom, cang);
	}

	@Override
	protected void calc_and_apply_fog(int shade, int vis, int pal) {
		if(!textureCache.isUseShader(globalpicnum)) {
			if (rendering == Rendering.Sprite && globalpal == 5 && globalshade == 127)
				shade = 0; // Blood's shadows (for pal 1)

			if(globalpal == 1 || pal == 1) {
				if (rendering == Rendering.Model) {
					shade = tspriteptr[Rendering.Model.getIndex()].shade;
					if(shade > 0)
						shade = BClipRange((int) (2.8f * shade), 32, 52);
				} else shade = 0;
			}
		}

		super.calc_and_apply_fog(shade, vis, pal);
	}

	@Override
	public TextureManager newTextureManager(Engine engine) {
		return new BloodTextureManager(engine);
	}

}
