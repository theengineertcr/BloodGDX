// This file is part of BloodGDX.
// Copyright (C) 2017-2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Factory;

import static com.badlogic.gdx.graphics.GL20.GL_SRC_ALPHA;
import static com.badlogic.gdx.graphics.GL20.GL_SRC_COLOR;
import static com.badlogic.gdx.graphics.GL20.GL_TEXTURE_2D;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static ru.m210projects.Build.Engine.curpalette;
import static ru.m210projects.Build.Engine.globalpal;
import static ru.m210projects.Build.Engine.globalshade;
import static ru.m210projects.Build.Engine.numshades;
import static ru.m210projects.Build.Engine.palookup;
import static ru.m210projects.Build.Render.Types.GL10.GL_COMBINE_ARB;
import static ru.m210projects.Build.Render.Types.GL10.GL_COMBINE_RGB_ARB;
import static ru.m210projects.Build.Render.Types.GL10.GL_INTERPOLATE_ARB;
import static ru.m210projects.Build.Render.Types.GL10.GL_OPERAND0_RGB_ARB;
import static ru.m210projects.Build.Render.Types.GL10.GL_OPERAND1_RGB_ARB;
import static ru.m210projects.Build.Render.Types.GL10.GL_OPERAND2_RGB_ARB;
import static ru.m210projects.Build.Render.Types.GL10.GL_PREVIOUS_ARB;
import static ru.m210projects.Build.Render.Types.GL10.GL_SOURCE0_RGB_ARB;
import static ru.m210projects.Build.Render.Types.GL10.GL_SOURCE1_RGB_ARB;
import static ru.m210projects.Build.Render.Types.GL10.GL_SOURCE2_RGB_ARB;
import static ru.m210projects.Build.Render.Types.GL10.GL_TEXTURE_ENV;
import static ru.m210projects.Build.Render.Types.GL10.GL_TEXTURE_ENV_MODE;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Architecture.BuildGdx;
import ru.m210projects.Build.Render.GLInfo;
import ru.m210projects.Build.Render.Polymost.Polymost.Rendering;
import ru.m210projects.Build.Render.TextureHandle.DummyTileData;
import ru.m210projects.Build.Render.TextureHandle.GLTile;
import ru.m210projects.Build.Render.TextureHandle.Hicreplctyp;
import ru.m210projects.Build.Render.TextureHandle.IndexedTileData;
import ru.m210projects.Build.Render.TextureHandle.PixmapTileData;
import ru.m210projects.Build.Render.TextureHandle.RGBTileData;
import ru.m210projects.Build.Render.TextureHandle.TextureManager;
import ru.m210projects.Build.Render.TextureHandle.TileData;

public class BloodTextureManager extends TextureManager {
	private GLTile dummy;
	private final float[] pal1_color = { 1.0f, 1.0f, 1.0f, 1.0f };

	public BloodTextureManager(Engine engine) {
		super(engine);
	}

	@Override
	public Color getshadefactor(int shade, int method) {
		if (Rendering.Skybox.getIndex() != 0 && globalpal == 1 && GLInfo.multisample != 0) {
			bindBloodPalette(shade);
			return super.getshadefactor(0, 0);
		}

		Color c = super.getshadefactor(shade, method);
		if (globalpal == 1)
			c.r = c.g = c.b = 1; // Blood's pal 1

		return c;
	}

	protected void bindDummyTexture() {
		if(dummy == null)
			dummy = new GLTile(new DummyTileData(1, 1), 1, false);
		bind(dummy);
	}

	@Override
	public void uninit() {
		if(dummy != null)
			dummy.delete();
		dummy = null;
		super.uninit();
	}

	public void bindBloodPalette(int shade) {
		BuildGdx.gl.glActiveTexture(++texunits);
		BuildGdx.gl.glEnable(GL_TEXTURE_2D);
		bindDummyTexture();

		pal1_color[3] = ((numshades - shade) / (float) numshades) + 0.1f;
		BuildGdx.gl.glTexEnvfv(GL_TEXTURE_ENV, 8705, pal1_color, 0); // GL_TEXTURE_ENV_COLOR

		BuildGdx.gl.glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_ARB);
		BuildGdx.gl.glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB_ARB, GL_INTERPOLATE_ARB);

		BuildGdx.gl.glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_RGB_ARB, GL_PREVIOUS_ARB);
		BuildGdx.gl.glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND0_RGB_ARB, GL_SRC_COLOR);

		BuildGdx.gl.glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE1_RGB_ARB, 34166); // GL_CONSTANT
		BuildGdx.gl.glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND1_RGB_ARB, GL_SRC_COLOR);

		BuildGdx.gl.glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE2_RGB_ARB, 34166); // GL_CONSTANT
		BuildGdx.gl.glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND2_RGB_ARB, GL_SRC_ALPHA);
	}

	@Override
	protected TileData loadPic(Hicreplctyp hicr, int dapicnum, int dapalnum, boolean clamping, boolean alpha,
			int skybox) {

		int expand = 1 | 2;
		if (hicr != null) {
			String fn = checkResource(hicr, dapicnum, skybox);
			byte[] data = BuildGdx.cache.getBytes(fn, 0);
			if (data != null) {
				try {
					return new PixmapTileData(new Pixmap(data, 0, data.length), clamping, expand);
				} catch (Throwable t) {
					t.printStackTrace();
					if (skybox != 0)
						return null;
				}
			}
		}

		if (shader != null)
			return new IndexedTileData(engine.getTile(dapicnum), clamping, alpha, expand);
		return new RGBTileData(engine.getTile(dapicnum), dapalnum, clamping, alpha, expand) {

			@Override
			protected int getColor(int dacol, int dapal, boolean alphaMode) {
				dacol &= 0xFF;
				if (alphaMode && dacol == 255)
					return curpalette.getRGBA(0, (byte) 0);

				if (dacol >= palookup[dapal].length)
					return 0;

				if (dapal == 1) {
					int shade = (min(max(globalshade, 0), numshades - 1));
					dacol = palookup[dapal][dacol + (shade << 8)] & 0xFF;
				} else
					dacol = palookup[dapal][dacol] & 0xFF;

				return curpalette.getRGBA(dacol, (byte) 0xFF);
			}
		};
	}
}
