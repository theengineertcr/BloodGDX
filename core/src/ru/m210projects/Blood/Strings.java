// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

public class Strings {
	public static final char[] frags =  new String("FRAG STATS").toCharArray();
	public static final char[] levelstats =  new String("LEVEL STATS").toCharArray();
	public static final char[] secrets = new String("SECRETS:").toCharArray();
	public static final char[] of = new String("OF").toCharArray();
	public static final char[] supersecret = new String("YOU FOUND A SUPER SECRET!").toCharArray();
	public static final char[] keycontinue = new String("PRESS A KEY TO CONTINUE").toCharArray();
	public static final char[] killsstat = new String("KILLS:").toCharArray();
	public static final char[] paused = new String("PAUSED").toCharArray();
	public static final char[] InterpolateRangeError = new String("fInterpolateRangeError").toCharArray();
	public static final char[] scrollmode = new String("MAP SCROLL MODE").toCharArray();
	public static final char[]	followmode = new String("MAP FOLLOW MODE").toCharArray();
	public static final char[]	loading = new String("LOADING").toCharArray();
	public static final char[]	cutskip = new String("Press ESC to skip").toCharArray();
	public static final char[]	wait = new String("please wait").toCharArray();
	public static final String Pickedup = "Picked up ";
	public static final char[] killsstat2 = new String("k:").toCharArray();
	public static final char[] secretsstat = new String("s:").toCharArray();
	
	public static final String	seq = "SEQ";
	public static final String	qav = "QAV";
	public static final String	flu = "FLU";
	public static final String	plu = "PLU";
	public static final String	pal = "PAL";
	public static final String	tlu = "TLU";
	public static final String	dat = "DAT";
	public static final String	raw = "RAW";
	public static final String	sfx = "SFX";
	public static final String	mid = "MID";
	public static final String	read = "r";
}
